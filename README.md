# Activity 1
1. Create a new file called assessment.md
2. For each of the items below, create a header section and write a small description about it.
3. After you add a response for each section below, commit the change for that section.
### Header Sections
- **Development Experience** What technologies have you been using recently?  Do you have a favorite language/tool?
- **Advice** What is the best advice you've received as it relates to your career?

Once you've completed each section, merge back into the master branch as a single commit.  Go to the numbers branch for the next section.